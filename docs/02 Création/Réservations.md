# Réservations

La commande Reservation permet de créer une réservation à l'intersection d'un élément structurel (Dalles/Murs/Poutres) et d'un réseau (Gaines/Canalisations/Chemins de câbles). Suivant la configuration prédéfinie avec la commande [Options], la réservation s'inserera avec une forme, un type, une épaisseur de rebouchage, un lot et/ou un numéro.

## GUIDE D'UTILISATION

1. Cliquez sur l’outil dans le ruban PIRO CIE

![Sélection Outil Réservation](../images/ToolBar_Réservation.PNG)

2. Sélection d'un élément structurel (Dalle/Mur/Poutre) :

![Sélection Support Réservation](../images/Résa_SelectSupport.PNG)

<span style="color:red">NOTA : Si les éléments structurels sont présents dans un lien, cochez l'option correspondantes dans les</span> [Options].

3. Sélection d'un Réseau (Gaine/Canalisation/Chemin de câble) :

![Sélection Support Réservation](../images/Resa_SelectMEP.PNG)

<span style="color:red">NOTA : Si les Réseaux sont présents dans un lien, cochez l'option correspondantes dans les</span> [Options].

4. Une réservation est créée à l'intersection du réseau et de la structure. Les options d'insertion sont respectée suivant les [Options].



![Résultat Outil Réservation Plan](../images/Resa_ResultatPlan.PNG "Title") ![Résultat Outil Réservation Plan](../images/Resa_ResultatCoupe.PNG) ![Résultat Outil Réservation Plan](../images/Resa_Propriete.PNG) 


"Vue en plan"



"Vue en coupe"


"Propriétés"

[Options] : <../01 Options>
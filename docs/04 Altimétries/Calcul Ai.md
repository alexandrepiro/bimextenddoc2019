# Calcul de l'Arrase Inférieure

Cette commande permet de calculer automatiquement la valeur de l'arrase inférieure pour l'ensemble des réservations présentes dans le projet.

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar Calcul Ai](../images/Ai/ToolBar.PNG)

#### 2. Le calcul s'effectue automatiquement pour l'ensemble des réservations

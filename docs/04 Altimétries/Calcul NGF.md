# Calcul du Nivellement Général Français (NGF)

Cette commande permet de calculer automatiquement la valeur de l'arrase inférieure pour l'ensemble des réservations présentes dans le projet avec pour origine le 0 du Nivellement Général Français (NGF).

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar Calcul Ai](../images/Ai/ToolBar.PNG)

#### 2. Le calcul s'effectue automatiquement pour l'ensemble des réservations



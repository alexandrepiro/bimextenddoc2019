# Position Automatique

Cette commande permet de mettre à jour automatiquement la position d'une réservation dès lors que le réseau qui la traverse est déplacé.

<span style="color:red"><b>Attention : cette commande est désactivée lorsque le travail est collaboratif afin de ne pas créer d'interférences involontaires entre les différents utilisateurs du projets.</b></span>

## Guide d'utilisation

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar position auto](../images/PosAuto/ToolBar.PNG)

#### 2. L'outil s'active s'il était inactif et inversement

![Position auto ON](../images/PosAuto/ON.PNG "Outil activé" )
![Position auto OFF](../images/PosAuto/OFF.PNG "Outil désactivé" )

#### 3. Cas où la réservation est traversée par plusieurs réseaux

Si la réservation contient plusieurs réseaux, et que l'un d'eux est déplacé, une boîte de dialogue apparaît :

![Position auto Dial](../images/PosAuto/dial.PNG)
- <b>Adapter la réservation existante</b> : la réservation actuelle est agrandie afin de continuer à contenir l'ensemble des réseaux.

- <b>Créer une nouvelle réservation pour les réseaux déplacés</b> : pour chaque réseau déplacé une nouvelle réservation est créée et la réservation existante s'adapte aux réseaux qu'elle contient encore.

- <b>Ne rien faire</b> : La réservation actuelle conserve ses dimensions et aucune nouvelle réservation n'est créée pour le ou les réseaux déplacés.


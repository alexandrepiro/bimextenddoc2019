# Annoter la vue

Cette commande permet de configurer les annotations qui seront associées aux réservations sur une vue.

<span style="color:red"><b>Attention : il est nécessaire de passer par cette commande pour paramétrer la commande Annoter Vue.</b></span>

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar Annotation](../images/Annotation/ToolBar.PNG)

#### 2. La boîte de dialogue suivante apparaît :

![Annotations boîte de dialogue](../images/Annotation/dial.PNG)

<span style="color:red"><b>Les champs doivent nécessairement être remplis afin que la commande Annoter Vue fonctionne.</b></span>

#### 3. Cliquer sur le bouton <span style="color:#4286f4"><b>A</b></span> pour accéder au choix de modèle d'annotation à utiliser.

#### 4. La boîte de dialogue suivante apparaît :

![Annotations boîte de dialogue](../images/Annotation/dial2.PNG)

Choisir parmis la liste suivante le modèle d'annotation à utiliser. Cliquer sur <b>Valider</b> pour terminer. 
# Configuration des annotations

Cette commande permet annoter les réservations sur une vue. Les vues qui peuvent être annoter à travers cette commande sont les vues en plan et les vues 3D.

<span style="color:red"><b>Attention : il est nécessaire de passer par la commande pour paramétrer la commande Annoter Vue.</b></span>

# GUIDE D'UTILISATION

#### 1. Cliquez sur l’outil dans le ruban <b>BIM Extend Resa</b>

![ToolBar Annotation](../images/Annotation/ToolBar.PNG)

#### 2. 